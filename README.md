# From 0 to monitoring in 30 minutes

This project is part of the Lightning Talk "[From zero to monitoring in 30 minutes](https://www.teamupit.nl/event/chapter-north-presents-lightning-talk-from-zero-to-monitoring-in-30-minutes/)" hosted by Team Rockstars IT

## Purpose

This project demonstrates how easy it is for Java Developers to add observability through monitoring for Spring Boot Applications.
This is accomplished by using three key components: Prometheus, Grafana and Micrometer.

# Key Components

## Prometheus

Prometheus is a time series database and will be used as the monitoring system.
This is where all the metrics will be stored.

> ### From metrics to insight
> Power your metrics and alerting with a leading open-source monitoring solution.

_Source: [prometheus.io](https://prometheus.io/)_

## Grafana

> ### The analytics platform for all your metrics
> Grafana allows you to query, visualize, alert on and understand your metrics no matter where they are stored. Create, explore, and share dashboards with your team and foster a data driven culture.

_Source: [grafana.com](https://grafana.com/grafana/)_

## Micrometer

> ### Vendor-neutral application metrics facade
> Micrometer provides a simple facade over the instrumentation clients for the most popular monitoring systems, allowing you to instrument your JVM-based application code without vendor lock-in. Think SLF4J, but for metrics.

_Source: [micrometer.io](https://micrometer.io)_

# Usage

## Run Tour of Rockstars 

First navigate to the `tour-of-rockstars-backend` module and then run the spring-boot app with the maven wrapper:

    cd tour-of-rockstars-backend 
    ./mvnw spring-boot:run

## Run Grafana and Prometheus

A docker-compose file with Grafana and Prometheus is provided. Open a new terminal at the root of the project and start the services with:

    docker-compose up -d

(Optional: modify the `.env` file to setup the SMTP settings for Grafana if you want to use alerts.)

## Add Prometheus as data source in Grafana

1. Open Grafana on [http://localhost:3000](http://localhost:3000) and go to `Configuration` > `Data Sources`:
   
    ![img/img.png](img/img.png)

2. Select Prometheus:

    ![img/img_1.png](img/img_1.png)

3. Enter `http://prometheus:9090` as URL, leave defaults and click `Save & Test`

    ![img/img_2.png](img/img_2.png)

## Import dashboard
    
1. Go to `Create` > `Import`

    ![img/img_4.png](img/img_4.png)

2. Select dashboard to import
    * To import the Tour of Rockstars Dashboard Demo, enter `14033`.
    
        See also: [https://grafana.com/grafana/dashboards/14033](https://grafana.com/grafana/dashboards/14033)
      
    * To import a dashboard for Prometheus (it is monitoring itself), enter `3662`.
    
        See also: [https://grafana.com/grafana/dashboards/3662](https://grafana.com/grafana/dashboards/3662)
      
    * To import a Spring Boot Statistics dashboard for generic JVM metrics from Micrometer, enter `6756`.
        
        See also: [https://grafana.com/grafana/dashboards/6756](https://grafana.com/grafana/dashboards/6756)

    ![img/img_3.png](img/img_3.png)

3. Click `Load` button

4. Select `Prometheus` instance (at the bottom) and click `Import`

    ![img/img_6.png](img/img_6.png)

## Start exploring and experimenting yourself.

---

# Credits

[yellow lightning png from pngtree.com](https://pngtree.com/so/yellow-lightning)