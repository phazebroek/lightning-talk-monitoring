package nl.phazebroek.tor;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Slf4j
@SpringBootApplication
public class TourOfRockstarsApplication {

    public static void main(String[] args) {
        SpringApplication.run(TourOfRockstarsApplication.class, args);
        log.info("Tour of Rockstars Backend is ready");
    }

}
