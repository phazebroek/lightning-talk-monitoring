package nl.phazebroek.tor.rockstar;

import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Entrypoint for the /rockstars resource.
 */
@RestController
@RequestMapping("/rockstars")
@RequiredArgsConstructor
public class RockstarController {

    private final RockstarService rockstarService;

    /**
     * Fetch the full line-up for the tour.
     */
    @GetMapping
    public List<Rockstar> getRockstars() {
        return rockstarService.getLineUp();
    }

    /**
     * Adds a Rockstar to the tour.
     */
    @PostMapping
    public ResponseEntity<String> join(@RequestBody Rockstar rockstar) {
        var success = rockstarService.add(rockstar);
        if (success) {
            return ResponseEntity.ok().body("Welcome to the tour " + rockstar.getName());
        }
        return ResponseEntity.status(HttpStatus.CONFLICT).body("Rockstar already joined!");
    }

    /**
     * Removes a Rockstar from the tour.
     *
     * @param name the name of the Rockstar to remove.
     */
    @DeleteMapping("/{name}")
    public ResponseEntity<String> leave(@PathVariable("name") String name) {
        var success = rockstarService.remove(name);
        if (success) {
            return ResponseEntity.ok().body("Good bye " + name + ", Keep on rocking!");
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Rockstar not found");
    }

}
