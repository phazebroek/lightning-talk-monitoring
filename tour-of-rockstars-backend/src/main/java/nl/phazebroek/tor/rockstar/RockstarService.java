package nl.phazebroek.tor.rockstar;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import javax.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.phazebroek.tor.monitoring.MonitoringService;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class RockstarService {

    private final MonitoringService monitoringService;

    /**
     * A simplified "storage" of all the rockstars on this tour.
     */
    private final Map<String, Rockstar> rockstars = new ConcurrentHashMap<>();

    @PostConstruct
    public void init() {
        add(new Rockstar().withName("Paul McCartney"));
        add(new Rockstar().withName("Mick Jagger"));
        add(new Rockstar().withName("James Hetfield"));
        add(new Rockstar().withName("Tom Araya"));
        add(new Rockstar().withName("Dave Mustaine"));
        add(new Rockstar().withName("Joey Belladonna"));
        add(new Rockstar().withName("Corey Taylor"));
        add(new Rockstar().withName("Till Lindemann"));
    }

    public List<Rockstar> getLineUp() {
        return new ArrayList<>(rockstars.values());
    }

    /**
     * Adds a rockstar to the tour.
     *
     * @return true when the rockstar was added successfully, false if it was already present.
     */
    public boolean add(Rockstar rockstar) {
        if (rockstars.containsKey(rockstar.getName())) {
            return false;
        }
        rockstars.put(rockstar.getName(), rockstar);
        monitoringService.incrementCounter("tor.rockstars.added.total");
        monitoringService.updateGauge("tor.rockstars.total", rockstars.size());
        log.info("{} joined the tour", rockstar.getName());
        return true;
    }

    public boolean remove(String name) {
        log.info("Removing {}", name);
        var isRemoved = rockstars.remove(name) != null;
        monitoringService.incrementCounter("tor.rockstars.removed.total");
        if (isRemoved) {
            monitoringService.updateGauge("tor.rockstars.total", rockstars.size());
        }
        return isRemoved;
    }
}
