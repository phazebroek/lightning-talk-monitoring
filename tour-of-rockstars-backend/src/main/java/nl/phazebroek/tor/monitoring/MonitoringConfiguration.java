package nl.phazebroek.tor.monitoring;

import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.autoconfigure.metrics.MeterRegistryCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Configuration class to set some default / common monitoring settings.
 */
@Configuration
public class MonitoringConfiguration {

    @Value("${spring.application.name}")
    private String applicationName;

    /**
     * Set default tags for all metrics. This is required for the imported JVM metrics dashboard in Grafana to work properly.
     */
    @Bean
    MeterRegistryCustomizer<MeterRegistry> metricsCommonTags() {
        return registry -> registry.config().commonTags("application", applicationName);
    }

}
