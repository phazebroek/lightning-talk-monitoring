package nl.phazebroek.tor.monitoring;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.Measurement;
import io.micrometer.core.instrument.MeterRegistry;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * This services provides some convenience methods to interact with the {@link MeterRegistry} and different {@link io.micrometer.core.instrument.Meter}
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class MonitoringService {

    /**
     * The actual registry for the meters. Used to create and store meters in.
     */
    private final MeterRegistry registry;

    /**
     * Keep a reference to every counter created. This prevents garbage collection and avoids the need to look them up every time.
     */
    private final Map<String, Counter> counters = new ConcurrentHashMap<>();

    private final Map<String, AtomicInteger> gauges = new ConcurrentHashMap<>();

    /**
     * Increments the counter with the given name. If the counter does not exists yet, it will be created automatically.
     */
    public void incrementCounter(String counterName) {
        var counter = getCounter(counterName);
        counter.increment();
        log.info("Incremented counter {} to {}", counterName, counter.count());
    }

    public void updateGauge(String name, int value) {
        var gauge = getGauge(name);
        gauge.set(value);
        log.info("Updated gauge {} to {}", name, value);
    }

    private Counter getCounter(String counterName) {
        return counters.computeIfAbsent(counterName, s -> createCounter(counterName));
    }

    private Counter createCounter(String counterName) {
        log.info("Creating counter: {}", counterName);
        return registry.counter(counterName);
    }

    private AtomicInteger getGauge(String name) {
        return gauges.computeIfAbsent(name, s -> createGauge(name));
    }

    private AtomicInteger createGauge(String name) {
        log.info("Creating gauge: {}", name);
        return registry.gauge(name, new AtomicInteger());
    }
}
